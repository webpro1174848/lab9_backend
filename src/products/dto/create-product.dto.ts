export class CreateProductDto {
  id: number;

  name: string;

  price: string;

  image: string;

  type: string;
}
